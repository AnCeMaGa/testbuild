const express = require ('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require ('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get('/apitechu/v1/hello',
    function(req,res){
      console.log("GET /apitechu/v1/hello" );
      res.send({"msg" : "Hola desde API TechU!" });
    }
  );


/****  Mi MiniPractica
app.get('/apitechu/v1/users',
      function(req,res){
        console.log("GET /apitechu/v1/users" );

      var users = require('./usuarios.json');

       var ind = 0;

       for (user of users) {
              if (ind < req.query.$top) {
               console.log(user);
                ind++;
              }else {
             break;
             }
         }

    if (req.query.$count == "true") {
      //  es el string true, no el bulenao true
          console.log("El numero total de usuarios es : " + users.length);
        }

        res.send({"msg" : "He escrito el fichero users2"});
   }
);

****/

/****
----------
Mini)PRÁCTICA

En GET USERS V1

Implementar los filtros $top y $count en el QUERY STRING de la petición

$top=5
-> Devuelve los 5 primeros usuarios

$count=true
-> Devuelve un campo "count" con el total de usuarios en la lista

! No el total de usuarios devueltos (pueden ser menos si se hace un top)

Se pueden combinar los dos

PISTA
Podemos devolver un object {} en lugar de un array []

Array.splice versus Array.slice

localhost:3000/apitechu/v1/users?$top=10&$count=true
----------


la suya   ****/

app.get("/apitechu/v1/users",
 function(req, res) {
   console.log("GET /apitechu/v1/users");
   console.log(req.query);

   var result = {};
   var users = require('./usuarios.json');

   if (req.query.$count == "true") {
     console.log("Count needed");
     result.count = users.length;
   }

   result.users = req.query.$top ?
      users.slice(0, req.query.$top) : users;

   res.send(result);
 }
);

//******/

app.post('/apitechu/v1/users',
          function(req,res){
            console.log("POST /apitechu/v1/users" );
            console.log("first_name es : " + req.body.first_name);
            console.log("last_name es : " + req.body.last_name);
            console.log("email es : " + req.body.email);

            var newUser = {
              "first_name" : req.body.first_name,
              "last_name" : req.body.last_name,
              "email" : req.body.email
            };

            var users = require('./usuarios.json');
            users.push(newUser);
            writeUserDataToFile(users);

            console.log("Usuario añadido con éxito");
            res.send({"msg" : "Usuario añadido con éxito"});
          }
        );

    app.delete("/apitechu/v1/users/:ind",
      function(req, res){
        console.log("DELETE /apitechu/v1/users/:ind");
        console.log("ind es " + req.params.ind);

        var users = require('./usuarios.json');


      for (user of users) {
             console.log("Length of array is " + users.length);
             if (user!= null && user.id == req.params.ind) {
               console.log("la id coincide");
              delete(users[user.id - 1]);
              // users.splice(user.id - 1, 1);
              break;
            }
        }

    /**************

        for (user of users) {
           console.log("Length of array is " + users.length);
             if (user != null && user.id == req.params.id) {
               console.log("La id coincide");
               delete users[user.id - 1];
               // users.splice(user.id - 1, 1);
               break;
             }
           }

           for (arrayId in users) {
             console.log("posición del array es " + arrayId);
             if (users[arrayId].id == req.params.id) {
               console.log("La id coincide");
               users.splice(arrayId, 1);
               break;
             }
           }

           users.forEach(function (user, index) {
             if (user.id == req.params.id) {
               console.log("La id coincide");
               users.splice(index, 1);
             }
           });

*****/

        writeUserDataToFile(users);

        console.log("Usuario " + req.params.ind + " borrado");
        res.send({"msg" : "Usuario borrado"});

      }
    )

    function writeUserDataToFile(data) {
      const fs = require('fs');
      var jsonUserData = JSON.stringify(data);

      fs.writeFile("./usuarios.json", jsonUserData, "utf8",
        function(err) {
          if (err) {
          console.log(err);
          }else {
          console.log("Datos escritos en fichero.");
          }
        }
      );
    }

    app.post("/apitechu/v1/monstruo/:p1/:p2",
       function(req,res) {
         console.log("POST /apitechu/v1/monstruo/:p1/:p2");

         console.log("Par�metros");
         console.log(req.params);

         console.log("Query String");
         console.log(req.query);

         console.log("Headers");
         console.log(req.headers);

         console.log("Body");
        console.log(req.body);

       }
)
