const express = require ('express');
const app = express();
const port = process.env.PORT || 3000;

const bodyParser = require ('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto " + port);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');

app.get('/apitechu/v1/hello',
    function(req,res){
      console.log("GET /apitechu/v1/hello" );
      res.send({"msg" : "Hola desde API TechU!" });
    }
  );

app.post("/apitechu/v1/monstruo/:p1/:p2",
   function(req,res) {
         console.log("POST /apitechu/v1/monstruo/:p1/:p2");

         console.log("Par�metros");
         console.log(req.params);

         console.log("Query String");
         console.log(req.query);

         console.log("Headers");
         console.log(req.headers);

         console.log("Body");
         console.log(req.body);

         }
  )

app.get("/apitechu/v1/users", userController.getUsersV1);
app.post("/apitechu/v1/users", userController.createUserV1);
app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);
app.post("/apitechu/v2/users", userController.createUserV2);

app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v1/logout', authController.logoutV1);




/********
   Código del profe :

---- LOGIN -----
app.post('/apitechu/v1/login',
 function(req, res) {
   var email = req.body.email;
   var password = req.body.password;

   if (!email || !password) {
     res.send(
       {
         "mensaje" : "Login incorrecto, necesarios email y password."
       }
     )
   };

   var loggedUserId;
   var users = require('./usuarios.json');
   for (user of users) {
     if (user.email == email && user.password == password) {
       console.log("User found, credentials correct");
       loggedUserId = user.id;
       user.logged = true;
       console.log("Logged in user with id " + user.id);
       io.writeUserDataToFile(users);
       break;
     }
   }

   var msg = loggedUserId ?
     "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

   var response = {};
   response.mensaje = msg;

   if (loggedUserId) {
     response.idUsuario = loggedUserId;
   }

   res.send(response);
 }
)


----LOGOUT-----
app.post('/apitechu/v1/logout',
 function(req, res) {
   var id = req.body.id;

   if (!id) {
     res.send(
       {
         "mensaje" : "Logout incorrecto, necesaria id de usuario."
       }
     )
   };

   var users = require('./usuarios.json');
   for (user of users) {
     if (user.id == id && user.logged === true) {
       console.log("User found, logging out");
       delete user.logged
       console.log("Logged out user with id " + user.id);
       io.writeUserDataToFile(users);
       break;
     }
   }

   var response = {
       "mensaje" : "Logout finalizado"
   };
   res.send(response);
 }
)
*******/
