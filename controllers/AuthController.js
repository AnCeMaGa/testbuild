
const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuacg6ed/collections/";
const mLabAPIKey = "apiKey=I2s6jMSheSntnb5BFErwsdgdBBbXr_xR";

function loginV1(req, res) {
  console.log("POST /apitechu/v1/login");
  var email = req.body.email;
  var password = req.body.password;
  if (!email || !password) {
    res.send(
      {
        "mensaje" : "Login incorrecto, necesarios email y password."
      }
    )
  };

  var loggedUserId;
  var users = require('../usuarios.json');
  for (user of users) {
    if (user.email == email && user.password == password) {
      console.log("User found, credentials correct");
      loggedUserId = user.id;
      user.logged = true;
      console.log("Logged in user with id " + user.id);
      io.writeUserDataToFile(users);
      break;
    }
  }

  var msg = loggedUserId ?
    "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

  var response = {};
  response.mensaje = msg;

  if (loggedUserId) {
    response.idUsuario = loggedUserId;
  }

  res.send(response);
}

function logoutV1(req, res) {
  console.log("POST /apitechu/v1/logout");
  var id = req.body.id;

  if (!id) {
    res.send(
      {
        "mensaje" : "Logout incorrecto, necesaria id de usuario."
      }
    )
  };

  var users = require('../usuarios.json');
  for (user of users) {
    if (user.id == id && user.logged === true) {
      console.log("User found, logging out");
      delete user.logged
      console.log("Logged out user with id " + user.id);
      io.writeUserDataToFile(users);
      break;
    }
  }
  var response = {
      "mensaje" : "Logout finalizado"
  };
  res.send(response);
}

/*****  este es mio

function loginV2(req, res) {
console.log("POST /apitechu/v2/users/:id");
var email = req.body.email;
var password = req.body.password;

var query = 'q={"email": "' + email + '"}';
console.log("query es " + query);

var httpClient = requestJson.createClient(baseMLabURL);
console.log("Client created");
httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body){
     if (err){
       var response = "Error obteniendo usuario";

       console.log("Error obteniendo usuario");
       res.status(500);
     } else{
       if (body.length > 0){
         var response = body [0];
         if (crypt.checkPassword(password, body[0].password)) {
             console.log("User found, credentials correct");
             query = 'q={"id" : ' + body[0].id + '}';
             var putBody = '{"$set":{"logged":true}}';
             loggedUserId = body[0].id;
             console.log("Logged in user with id " + body[0].id);
             httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, body2PUT){
             var response = " Usuario logado"
                }
             );
           }
       }else{
         var response = {
           "msg" : "Usuario No encontrado"
         }
         console.log("Usuario No encontrado");
         res.status(404);
       }
     }
   }
 );
   res.send(response);
};

function logoutV2(req, res) {

console.log("POST /apitechu/v2/users/:id");
var id = req.body.id;

  if (!id) {
    var response = "Logout incorrecto, necesaria id de usuario.";
  };

  var query = 'q={"id": ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body){
    if (err){
      var response = {
        "msg" : "Error obteniendo usuario"
      }
      console.log("Error obteniendo usuario");
      res.status(500);
    } else{
      if (body.length > 0 && body.logged) {
            console.log("User found, logado");
            query = 'q={"id" : ' + body[0].id + '}';
            var putBody = '{"$unset":{"logged":""}}';
            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(err, resMLab, body2){
               var response = " Usuario logado"
                  }
               )
            }
      var response = body [0];
      } else {
        var response = {
          "msg" : "Usuario No encontrado"
        }
        console.log("Usuario No encontrado");
        res.status(404);
      }
    }

  var response = "Logout finalizado"
    res.send(response);
}

*****/


//***** del profe
function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");
 var email = req.body.email;
 var password = req.body.password;

 var query = 'q={"email": "' + email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
       crypt.checkPassword(password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (body.length == 0 || !isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

//****////


module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
