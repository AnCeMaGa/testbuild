const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

// para que sea autoncontenido - lanza la API
var server = require('../server');


describe('First unit test',
  function() {
      it('Test that Duckduckgo works', function(done) {
          chai.request('http://www.duckduckgo.com')
          // en el recurso ---- vete (get) a ...raiz(/)
            .get('/')
         // al terminar ejecuta esa funcion
            .end(
                function(err, res) {
                  console.log("Request has finished");
              //    console.log(err);
              //   console.log(res);
                  res.should.have.status(200);
                  done();
                }
            )
      }
    )
  }
)

describe('Test de API Usuarios',
  function() {
      it('Prueba que la API de Usuarios responde', function(done) {
          chai.request('http://localhost:3000')
            .get('/apitechu/v1/hello')
            .end(
                function(err, res) {
                  console.log("Request has finished");
                  res.should.have.status(200);
                  done();
                }
            )
      }
    ),
    it('Prueba que la API devuelve una lista de Usuarios correcta', function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/login')
          .end(
              function(err, res) {
                console.log("Request has finished");
                res.should.have.status(200);
                res.body.login.should.be.a("array");

                for (user of res.body.users) {
                      user.should.have.property('email');
                      user.should.have.property('password');
                }
                done();
              }
          )
        }
     )
  }
)
