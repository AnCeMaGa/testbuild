const fs = require('fs');

function writeUserDataToFile(data) {
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err) {
      if (err) {
      console.log(err);
      }else {
      console.log("Datos escritos en fichero.");
      }
    }
  );
}

/*** expone algo que tenemos declarado **/
module.exports.writeUserDataToFile = writeUserDataToFile;
